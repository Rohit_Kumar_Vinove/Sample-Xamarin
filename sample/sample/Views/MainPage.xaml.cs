﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using sample.Interface;

namespace sample.Views
{
    public partial class MainPage : ContentPage
    {
        public bool emailscale = true;

        public bool passscale = true;
        #region constructor

        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        
        }

        #endregion Constructor

        #region lebel Password got focus

        private async void lblpassword_Focused(object sender, FocusEventArgs e)
        {
            passscale = false;

            await rootlayout.TranslateTo(0, -90, 100,Easing.SinIn);
          
             await imgLogo.ScaleTo(0.7, 100, Easing.SinIn);   
                      
            cancelImagepassword.Source = ImageSource.FromFile("cancel.png");

            if (lblpassword.Text == null)
            {
                cancelImagepassword.IsVisible = false;
            }
            else
            {
                cancelImagepassword.IsVisible = true;
            }
            
        }
        #endregion  End of lebel Password got focus

        #region label Email Got Focus

        private async void lblemailid_Focused(object sender, FocusEventArgs e)
        {
            emailscale = false;

            
            await rootlayout.TranslateTo(0, -90, 100, Easing.SinIn);

                await imgLogo.ScaleTo(0.7, 100, Easing.SinIn);
                      
            cancelImageMail.Source = ImageSource.FromFile("cancel.png");

            if (lblpassword.Text == null)
            {
                cancelImageMail.IsVisible = false;
            }
            else
            {
                cancelImageMail.IsVisible = true;
            }
           
        }

        #endregion label Email Got Focus

        #region lebelPassword Textchange
        private void lblpassword_TextChanged(object sender, TextChangedEventArgs e)
        {

         
            if (lblpassword.Text == "")
            {
                cancelImagepassword.IsVisible = false;
            }
            else
            {
                cancelImagepassword.IsVisible = true;
            }
            
        }
        #endregion lebelPassword Textchange

        #region lebelemail Textchnaged
        private void lblemailid_TextChanged(object sender, TextChangedEventArgs e)
        {
           // cancelImageMail.Source = ImageSource.FromFile("cancel.png");

            if (lblemailid.Text=="")
            {
                cancelImageMail.IsVisible = false;
            }
            else
            {
                cancelImageMail.IsVisible = true;
            }
          
        }
        #endregion lebelemail Textchnaged

        #region label Email Unfouced Event
        private async void lblemailid_Unfocused(object sender, FocusEventArgs e)
        {
            emailscale = true;
         
            await rootlayout.TranslateTo(0, 0, 100,Easing.SinOut);

            if (passscale)
            {
                await imgLogo.ScaleTo(1, 100, Easing.SinOut);
            }
        
            cancelImageMail.Source = ImageSource.FromFile("cancel.png");

            cancelImageMail.IsVisible = false;
        }
        #endregion label Email Unfouced Event


        #region label password Unfouced Event
        private async void lblpassword_Unfocused(object sender, FocusEventArgs e)
        {
            passscale = true;
          
            await rootlayout.TranslateTo(0, 0, 100,Easing.SinOut);

            if (emailscale)
            {
                await imgLogo.ScaleTo(1, 100, Easing.SinOut);
            }
            cancelImagepassword.Source = ImageSource.FromFile("cancel.png");

            cancelImagepassword.IsVisible = false;
        }
        #endregion label password Unfouced Event


        #region on Image tapped Email
        private void OncancelImageMail_Pressed(object sender, EventArgs e)
        {
          
            cancelImageMail.Source = ImageSource.FromFile("cancel_ontap.png");

            cancelImageMail.IsVisible = true;
            clearText("email");
        }
        #endregion on Image tapped Email


        #region on Image tapped password
        private void OncancelImagepassword_Tapped(object sender, EventArgs e)
        {
            cancelImagepassword.Source= ImageSource.FromFile("cancel_ontap.png");
            cancelImagepassword.IsVisible = true;
            clearText("password");
        }
        #endregion on Image tapped password


        #region method clear text
        private async void clearText(string text ) {
            await Task.Delay(300);
           
            if (text.Equals("email"))
            {
                lblemailid.Text = "";
            }
            else { lblpassword.Text = ""; }
            
        }
        #endregion method clear text


        #region SignOut
        private void SignIn_Clicked(object sender, EventArgs e)
        {
            loginLoder.IsRunning = true;

            string _result = lblpassword.Text;

            var IsEmailValid=  EmailValidatorBehavior.isValid;

            if(IsEmailValid==true && !(string.IsNullOrEmpty(_result)))
            {
                DependencyService.Get<IToast>().ShowToast("Login Sucessfully..");
            }
            else
            {
                Navigation.PushPopupAsync(new PopsupPage());

                loginLoder.IsRunning = false;
            }          
            
        }
        #endregion SignOut
    }
}
