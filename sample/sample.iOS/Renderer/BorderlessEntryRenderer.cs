﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using sample;
using sample.iOS.Renderer;
using System.ComponentModel;
using sample.Controls;
using CoreGraphics;

[assembly: ExportRenderer(typeof(BorderlessEntry), typeof(BorderlessEntryRenderer))]
namespace sample.iOS.Renderer
{
    public class BorderlessEntryRenderer : EntryRenderer
    {
        private nfloat width;

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            Control.Layer.BorderWidth = 0;
            Control.BorderStyle = UITextBorderStyle.None;
          //  Control.Frame = new CGRect(0, 0, width, 80);
        }
    }
}